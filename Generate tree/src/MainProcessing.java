
import java.io.BufferedWriter;
import java.io.FileNotFoundException;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import java.lang.Math;
import java.util.Random;

public class MainProcessing {

	public String FilePath = "E:\\savefile1";
	private String FileTye = ".txt";
	
	private ArrayList<Node> nodelist;
	private ArrayList<Node> parentnodelist;

	int[] datasize = { 15, 20, 25, 30 };

	int state = 3;
	/* for ten node */
//	int[] dealine1 = { 32, 64, 128 };
//	int[] dealine2 = { 16, 32, 64};
//	int[] dealine3 = { 16, 32 };
//	private int numberofnode = 10;
//	private int maxdepth = 4;
//	private int maxchild = 3;
//	private int numberoftree = 10;
	/* for 20 nodes */
//	int[] dealine1 = { 32, 64, 128 };
//	int[] dealine2 = { 16, 32, 64};
//	int[] dealine3 = { 16, 32 };
//	private int numberofnode = 20;
//	private int maxdepth = 5;
//	private int maxchild = 3;
//	private int numberoftree = 30;
	
	/* for 30 node */
//	int[] dealine1 = { 32, 64, 128 };
//	int[] dealine2 = { 16, 32,64};
//	int[] dealine3 = { 16, 32 };
//	private int numberofnode = 30;
//	private int maxdepth = 5;
//	private int maxchild = 3;
//	private int numberoftree = 50;
	/* for 40 node */
	int[] dealine1 = { 32, 64, 128 };
	int[] dealine2 = { 16, 32, 64};
	int[] dealine3 = { 8, 16 };
	private int numberofnode = 9;
	private int maxdepth = 5;
	private int maxchild = 3;
	private int numberoftree = 1;
	public MainProcessing() throws IOException {
		FilePath += numberofnode+"_"+state+ "_"+FileTye;
		nodelist = new ArrayList<>();
		parentnodelist = new ArrayList<>();
		process();
	}

	private int getmaxnumbernode(int maxdepth, int maxchild) {
		int temp = 0;
		for (int i = 0; i <= maxdepth; i++) {
			temp += (int) Math.pow(maxchild, i);
		}
		return temp;
	}

	public void process() throws IOException {
		int temp = getmaxnumbernode(maxdepth, maxchild);
		if (temp < numberofnode) {
			System.out.println("");
			return;
		}
		Random rand = new Random();
		int id;
		int sizeindex;
		int deadline;
		for (int j = 0; j < numberoftree; j++) {
			Node sinknode = new Node(0, 0, 0);
			nodelist.add(sinknode);
			parentnodelist.add(sinknode);
			for (int i = 0; i < numberofnode; i++) {
				sizeindex = rand.nextInt(datasize.length);
				switch (state) {
				case 1:
					deadline = dealine1[rand.nextInt(dealine1.length)];
					break;
				case 2:
					deadline = dealine2[rand.nextInt(dealine2.length)];
					break;
				case 3:
					deadline = dealine3[rand.nextInt(dealine3.length)];
					break;
				default:
					deadline = 0;
					break;
				}
				
				Node tempnode = new Node(i + 1, deadline, datasize[sizeindex]);
				nodelist.add(tempnode);
				id = rand.nextInt(parentnodelist.size());
				tempnode.setParent(parentnodelist.get(id));

				if (parentnodelist.get(id).getToatalNumberChildern() == numberofnode) {
					parentnodelist.remove(id);
				}
				if (tempnode.getHopCount() != maxdepth) {
					parentnodelist.add(tempnode);
				}
				System.out.println(tempnode.getnodeId() + "-" + tempnode.parent.getnodeId());
			}
			System.out.println();
			System.out.println();
			savetofile(FilePath);
			nodelist.clear();
			parentnodelist.clear();
		}
	}

	private void savetofile(String filePath) throws IOException {
		BufferedWriter writer = null;
		String temp = "";
		try {
			writer = new BufferedWriter(new FileWriter(filePath, true));
			for (int i = 0; i < nodelist.size(); i++) {
				temp += nodelist.get(i).getDeadline() + " ";
			}
			temp.trim();
			writer.write(temp);
			writer.newLine();
			temp = "";
			for (int i = 1; i < nodelist.size(); i++) {
				temp += nodelist.get(i).getnodeId() + "-" + nodelist.get(i).parent.getnodeId() + " ";
			}
			temp.trim();
			writer.write(temp);
			writer.newLine(); // Add new line
			temp = "";
			for (int i = 0; i < nodelist.size(); i++) {
				temp += nodelist.get(i).getDatalength() + " ";
			}
			temp.trim();
			writer.write(temp);
			writer.newLine(); // Add new line
			temp = "";
			// Add new line
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {

				if (writer != null) {
					writer.close();
				}

			} catch (IOException ex) {

				ex.printStackTrace();

			}
		}

	}

}
