public class Transmission {
	private int senderId;
	private int receiverId;
	private int a;
	private int b;
	private String transmissionString;
	private String transmissionStringAlgorithm;
	private float priority;
	private int nextdeadline;
	private int deadline;
	private int deadlineValue;
	public int conflictTransmission;
	private int time;

	public int getB() {
		return b;
	}

	public float getPriority() {
		return priority;
	}

	public void setPriority(float priority) {
		this.priority = priority;
	}

	public int getConflictTransmission() {
		return conflictTransmission;
	}

	public void setConflictTransmission(int conflictTransmission) {
		this.conflictTransmission = conflictTransmission;
	}

	public int getSenderId() {
		return senderId;
	}

	public void setSenderId(int senderId) {
		this.senderId = senderId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}
    
	
	public void updateA(){
		
	}
	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getTime() {
		return time;
	}

	public String getTransmissionString() {
		return transmissionString;
	}
	
	public String getTransmissionStringOld() {
		return senderId+"-"+receiverId;
	}
	public void setTransmissionString(String transmissionString) {
		this.transmissionString = transmissionString;
	}

	public Transmission(int senderId, int receiverId, int hopcountSender, int hopcountReciever, int deadLine,
			int time) {
		this.transmissionString = "(" + senderId + "-" + receiverId + ")" + time;
		this.transmissionStringAlgorithm = "T(" + senderId + "-" + (hopcountReciever) + ")-" + time;
		this.senderId = senderId;
		this.receiverId = receiverId;
		a = hopcountSender - hopcountReciever + (time - 1) * deadLine;
		this.deadline = deadLine * time;
		this.deadlineValue = deadLine;
		b = this.deadline - hopcountReciever;
		this.time = time;
	}

	public String getTransmissionStringAlgorithm() {
		return transmissionStringAlgorithm;
	}

	public void setTransmissionStringAlgorithm(String transmissionStringAlgorithm) {
		this.transmissionStringAlgorithm = transmissionStringAlgorithm;
	}

	public int timeWindowValue() {
		return b - a;
	}

	public void UpdateValueA() {
		a++;
	}

	public int getNextdeadline() {
		return nextdeadline;
	}

	public void updateDeadline() {
		a += deadline;
		b += deadline;
		nextdeadline += deadline;
	}

	public boolean isAddActive(int timeSlot) {
	
		if (timeSlot <= (time - 1) * deadlineValue) {
			return false;
		}
		return true;
	}

}