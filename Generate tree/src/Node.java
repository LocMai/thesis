import java.util.ArrayList;
import java.util.List;

public class Node {

	Node parent;
	ArrayList<Node> children;
	int nodeId;
	int hopCount;
	float priority;
	int deadline;
	int totalNumberChildern;
	ArrayList<String> dataInNode;
	int datalength;

	public Node(int id, int deadLine) {
		nodeId = id;
		children = new ArrayList<>();
		dataInNode = new ArrayList<>();
		dataInNode.add("" + nodeId);
		this.deadline = deadLine;
	}
	
	public Node(int id, int deadLine, int data_length) {
		nodeId = id;
		children = new ArrayList<>();
		dataInNode = new ArrayList<>();
		dataInNode.add("" + nodeId);
		this.deadline = deadLine;
		this.datalength = data_length;
	}
    
	public int getDatalength() {
		return datalength;
	}

	public void setDatalength(int datalength) {
		this.datalength = datalength;
	}

	public int getHopCount() {
		return hopCount;
	}

	public void setHopCount(int hopCount) {
		this.hopCount = hopCount;
	}

	public int getDeadline() {
		return deadline;
	}

	public void setDeadline(int deadline) {
		this.deadline = deadline;
	}

	public int getToatalNumberChildern() {
		return children.size();
	}

	public void setToatalNumberChildern(int toatalNumberChildern) {
		this.totalNumberChildern = toatalNumberChildern;
	}

	public void sendData(String data) {
		if (dataInNode.contains(data)) {
			if (parent != null) {
				System.out.println("node: " + nodeId + "send data:" + data + " to " + "node" + parent.nodeId);
				System.out.print("node: " + nodeId + "have data");
				try {
					dataInNode.remove(data);
				} catch (Exception e) {
					// TODO: handle exception
				}
				for (String string : dataInNode) {
					System.out.print("||" + string);
				}
				System.out.println();
				parent.recieveData(data);
				System.out.println();
			}
		} else {
			//System.out.println("node: " + nodeId + "dont have data from" + data);
		}
	}

	public void recieveData(String data) {
		dataInNode.add(data);
		System.out.print("Node: " + nodeId + "have data:");
		for (String string : dataInNode) {
			System.out.print("||" + string);
		}
		System.out.println();
	};

	private void addChildren(Node node) {
		children.add(node);
	}

	public void setParent(Node node) {
		parent = node;
		node.addChildren(this);
		hopCount = node.hopCount + 1;
		updateHopCount();
	}

	public void updateHopCount() {
		for (Node node : children) {
			node.hopCount = hopCount + 1;
			node.updateHopCount();
		}
	}

	public int getnodeId() {
		return nodeId;
	}

	public void printTree() {
		if (parent != null)
			System.out.println("node" + nodeId + "have parent" + parent.getnodeId() + " havechildren");
		for (int i = 0; i < children.size(); i++) {
			System.out.print(children.get(i).nodeId);
		}
		System.out.println();
	}

	public void getNumberOfNOdeInBranch() {
		calculateNumberOfNOde(children);
		System.out.println(" node " + nodeId + " have " + totalNumberChildern + " children");
	}

	public void calculateNumberOfNOde(ArrayList<Node> children) {
		if (children.size() != 0) {
			totalNumberChildern += children.size();
			for (Node node : children) {
				calculateNumberOfNOde(node.children);
			}
		}
	}

	public void update() {
		deadline++;
	}

	public int getBvalue() {
		return deadline - hopCount;
	}
}
